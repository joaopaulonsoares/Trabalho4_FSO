#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include "libsen.h"

int main(int argc, char *argv[ ]){
  double angulo =0.0;
  double resultado = 0.0;
  sscanf(argv[2], "%lf", &angulo);

  void *handle = dlopen("libsen.so",RTLD_LAZY);
  void (*seno)  = dlsym(handle, "seno");

  printf("Angulo: %lf, Opcao: %s\n",angulo,argv[1]);
  if(strcmp("-s",argv[1])==0){
    resultado =  (seno)(angulo);
    dlclose(handle);
    printf("seno (%.2lf) = %.3lf\n",angulo,resultado);
  }else if(strcmp("-a",argv[1])==0) {

  }

  return 0;
}
