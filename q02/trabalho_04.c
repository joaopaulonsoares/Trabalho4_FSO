#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libsen.h"

int main(int argc, char *argv[ ]){
  double angulo =0.0;
  double resultado = 0.0;
  sscanf(argv[2], "%lf", &angulo);

  printf("Angulo: %lf, Opcao: %s\n",angulo,argv[1]);
  if(strcmp("-s",argv[1])==0){
    resultado =  seno(angulo);
    printf("seno (%.2lf) = %.3lf\n",angulo,resultado);
  }else if(strcmp("-a",argv[1])==0) {
    resultado = arc_seno(angulo);
    printf("arcseno (%.3lf) = %.3lf\n",angulo,resultado);
  }

  return 0;
}
