#ifndef OPERACOESGEOMETRICAS_H
#define OPERACOESGEOMETRICAS_H

int fatorial(int number);
int doublefactorial(int n);
double seno(double angulo);
double arc_seno(double angulo);
#endif
