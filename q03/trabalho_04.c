#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include "libsen.h"

int main(int argc, char *argv[ ]){
  double angulo =0.0;
  double senoValue = 0.0;
  double resultado = 0.0;


  void* handle = dlopen("./libsen.so",RTLD_LAZY);
  if(!handle){
    printf("ERROO\n" );
    exit(1);
  }

  double (*seno)(double)  = dlsym(handle, "seno");
  double (*arc_seno)(double)  = dlsym(handle, "arc_seno");

  if(strcmp("-s",argv[1])==0){
    sscanf(argv[2], "%lf", &angulo);
    resultado =  (*seno)(angulo);
    printf("seno (%.2lf) = %.3lf\n",angulo,(*seno)(angulo));
  }else if(strcmp("-a",argv[1])==0) {
    sscanf(argv[2], "%lf", &senoValue);
    resultado =  (*arc_seno)(senoValue);
    printf("arcseno (%.3lf) = %.2lf\n",senoValue,resultado);
  }

  dlclose (handle);

  return 0;
}
