#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "libsen.h"
#define PI 3.14

int fatorial (int number)  {
  if(number == 0){
    return 1;
  }
  if(number==1){
      return number;
  }
  return fatorial(number-1) * number;
}

int doublefactorial(int number){
   int i=0;
   double res=1.0;

   for(i=number;i>=1;i-=2){
     res *=i;
   }

   return res;
}

double seno(double angulo){
  double sinValue = 0.0;

  for(int i=0;i<15;i++){
    sinValue = sinValue + (double)((pow(-1, i) / fatorial(2*i + 1)) * pow(angulo, 2*i+1));
  }

  return sinValue;
}

double arc_seno(double seno){
  double arc_senoValue =0.0;

  for(int i=0;i<15;i++){
    arc_senoValue = arc_senoValue +  ( (double)doublefactorial( 2*i-1) / (double)doublefactorial(2*i)  ) * ( pow(seno,2*i+1) / (2*i+1) );
  }

  return arc_senoValue;
}
